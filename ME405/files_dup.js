var files_dup =
[
    [ "controller_TP.py", "controller__TP_8py.html", "controller__TP_8py" ],
    [ "EncoderDriver_Lab8.py", "EncoderDriver__Lab8_8py.html", "EncoderDriver__Lab8_8py" ],
    [ "EncoderDriver_TP.py", "EncoderDriver__TP_8py.html", "EncoderDriver__TP_8py" ],
    [ "Lab1.py", "Lab1_8py.html", "Lab1_8py" ],
    [ "LEDRun.py", "LEDRun_8py.html", [
      [ "LED", "classLEDRun_1_1LED.html", "classLEDRun_1_1LED" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_TP.py", "main__TP_8py.html", "main__TP_8py" ],
    [ "mainLab3.py", "mainLab3_8py.html", "mainLab3_8py" ],
    [ "mainLab4.py", "mainLab4_8py.html", "mainLab4_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "MotorDriver_Lab8.py", "MotorDriver__Lab8_8py.html", "MotorDriver__Lab8_8py" ],
    [ "MotorDriver_TP.py", "MotorDriver__TP_8py.html", "MotorDriver__TP_8py" ],
    [ "position_TP.py", "position__TP_8py.html", "position__TP_8py" ],
    [ "tscreen.py", "tscreen_8py.html", "tscreen_8py" ],
    [ "UIFrontEnd.py", "UIFrontEnd_8py.html", "UIFrontEnd_8py" ]
];