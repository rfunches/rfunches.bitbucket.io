var classMotorDriver__Lab8_1_1MotorDriver =
[
    [ "__init__", "classMotorDriver__Lab8_1_1MotorDriver.html#a9811a6e90e7ca4f9da414667457b920a", null ],
    [ "define_interrupts", "classMotorDriver__Lab8_1_1MotorDriver.html#a89a41d12144bc8636e4d21b6f4a9e85f", null ],
    [ "disable", "classMotorDriver__Lab8_1_1MotorDriver.html#a774106dbcad3a6d31ee3f128c6e35a31", null ],
    [ "enable", "classMotorDriver__Lab8_1_1MotorDriver.html#a244707699c3f69785ad4311bc52a5edc", null ],
    [ "Fault_Cleared", "classMotorDriver__Lab8_1_1MotorDriver.html#a1efaca7bc3a6ca763056ceaf0ec1af00", null ],
    [ "nFault", "classMotorDriver__Lab8_1_1MotorDriver.html#a6b835c6cd3f13693b890c15726ef135e", null ],
    [ "set_duty", "classMotorDriver__Lab8_1_1MotorDriver.html#a6c9f4b392d3f0ad0a9c8c91a06dc9816", null ],
    [ "ButtonInt", "classMotorDriver__Lab8_1_1MotorDriver.html#a880bb8173a0f25558cb1b59f90054108", null ],
    [ "duty", "classMotorDriver__Lab8_1_1MotorDriver.html#a69260046c0fa18494fea067bd2c67fd3", null ],
    [ "motor_number", "classMotorDriver__Lab8_1_1MotorDriver.html#a1522fc834b97792ecad313133660365e", null ],
    [ "nFaultInt", "classMotorDriver__Lab8_1_1MotorDriver.html#a7730cabdd09f0fc84581867788c88d4c", null ],
    [ "pin_Button", "classMotorDriver__Lab8_1_1MotorDriver.html#a5bc6e1b1f81a7daa99d86e638c695278", null ],
    [ "pin_IN1", "classMotorDriver__Lab8_1_1MotorDriver.html#a39248710fac6c5d41eb2a6caaecca766", null ],
    [ "pin_IN2", "classMotorDriver__Lab8_1_1MotorDriver.html#aab7a9af6ed6d77221ab4c6e5ae99e9eb", null ],
    [ "pin_nFault", "classMotorDriver__Lab8_1_1MotorDriver.html#a5249eed1147990e7a9a1d47be6653807", null ],
    [ "pin_nSLEEP", "classMotorDriver__Lab8_1_1MotorDriver.html#a7748041d6271f160ec92b67fd82f5ce4", null ],
    [ "t3ch1", "classMotorDriver__Lab8_1_1MotorDriver.html#acff075e6882981552f8e95432fe08d16", null ],
    [ "t3ch2", "classMotorDriver__Lab8_1_1MotorDriver.html#a580aed13f1fd267e5a198668826ff9a2", null ],
    [ "timer", "classMotorDriver__Lab8_1_1MotorDriver.html#abad39a732c82d6a6d0bc7da9395b58d1", null ]
];